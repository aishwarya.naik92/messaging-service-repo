
const {Datastore} = require('@google-cloud/datastore')
const express = require('express')
const cors = require('cors')
const axios = require('axios');

const app = express();
app.use(express.json())
app.use(cors())

const datastore = new Datastore()

//create a new message
// also verify if sender & receiver both exist
app.post('/messages', async(req, res) => {

    const key = datastore.key('messagesStore');
    const obj = req.body;

    const senderVerification = "http://wedding-planner-authorization-service.wedding-planner-324818.wl.r.appspot.com/employees/"+obj.sender+"/verify";
    const response1 = await axios.get(senderVerification);
    const obj1 = response1.data;

    if(obj1.error){
        res.send({error: "Sender could not be verified"});
    }

    const receiverVerification = "http://wedding-planner-authorization-service.wedding-planner-324818.wl.r.appspot.com/employees/"+obj.receiver+"/verify";
    const response2 = await axios.get(receiverVerification);
    const obj2 = response2.data;

    if(obj2.error){
        res.send({error: "Receiver could not be verified"});
    }

    const response = await datastore.save({key:key,data:obj})
    res.send(response);
})


//get message by id
app.get('/messages/:mid', async(req,res) =>{
    const mid = req.params.mid;
    const query = datastore.createQuery('messagesStore')
    const [data]   = await datastore.runQuery(query);
    let output = "";
    for(let eachData of data)
    {
        console.log(eachData[datastore.KEY])
        if(eachData[datastore.KEY].name === mid)
        {
            output = eachData;
            break;
        }
    }

    res.send(output);
})


//get messages
// - by receiver only 
// - by sender only 
// - by sender and receiver
// - without sender and messages - get all messages
app.get('/messages', async (req, res)=>{
    const sender = req.query.sender;

    const receiver = req.query.receiver;

    if(sender !== undefined && receiver === undefined)
    {
        const query = datastore.createQuery('messagesStore')
        const [data]   = await datastore.runQuery(query);
        let output = "";
        for(let eachData of data)
        {
            if(eachData.sender === sender)
            {
                output = eachData;
                break;
            }
        }
        
        res.send(output);
    }
    else if(receiver !== undefined && sender === undefined)
    {
        const query = datastore.createQuery('messagesStore')
        const [data]   = await datastore.runQuery(query);
        let output = "";
        for(let eachData of data)
        {
            if(eachData.receiver === receiver)
            {
                output = eachData;
                break;
            }
        }
        res.send(output);
    }
    else if(sender !== undefined && receiver !== undefined)
    { 
        const query = datastore.createQuery('messagesStore').filter("receiver","==",receiver).filter("sender", "=", sender)// get all 
        const [data]   = await datastore.runQuery(query);
        let output = "";
        for(let eachData of data)
        {
            if(eachData.receiver === receiver && eachData.sender === sender)
            {
                output = eachData;
                break;
            }
        }
        res.send(output);
    }
    else {
        const query = datastore.createQuery('messagesStore')
        const [data]   = await datastore.runQuery(query);
        res.send(data);
    }
});


const PORT = process.env.PORT || 3002;
app.listen(PORT,()=>console.log('Application Started'));